<?php
$students = [165, 170, 168, 169, 185, 198, 150, 177];
$names = ['Владимиров', 'Кузнецова', 'Гайдай', 'Удалов' , 'Абдувалиев', 'Эркинбеков' ,'Арзыбаев', 'Маркешин'];

$array = [];

for ($i = 0; count($students) > count($array); $i++){
    $array += [$students[$i] => $names[$i]];
}
ksort($array);

echo end($array) . "\n";